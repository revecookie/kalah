package com.backbase.kalah.model;

import com.backbase.kalah.model.domainerror.InvalidMoveException;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class Game {

    private List<Pit> pits = new ArrayList<>();
    private Player turn = Player.FIRST;

    public Game() {
        for (int i = 1; i < 7; i++) {
            pits.add(new Pit(6, i, Player.FIRST));
        }

        pits.add(new Pit(0, 7, Player.FIRST));

        for (int i = 8; i < 14; i++) {
            pits.add(new Pit(6, i, Player.SECOND));
        }

        pits.add(new Pit(0, 14, Player.SECOND));
    }

    public void move(Integer pitNumber) throws InvalidMoveException {

        if (isGameOver() || !isPitNumberValid(pitNumber)) throw new InvalidMoveException();

        if (turn.equals(Player.FIRST)) {

            moveFirstPlayer(pitNumber);
            turn = Player.SECOND;
        } else {
            moveSecondPlayer(pitNumber);
            turn = Player.FIRST;
        }


    }

    public Boolean isGameOver() {
        Integer firstPlayerPits = pits
                .stream()
                .filter(p -> p.getPlayer().equals(Player.FIRST) && p.getStones().equals(0))
                .collect(Collectors.toList())
                .size();

        Integer secondPlayerPits = pits
                .stream()
                .filter(p -> p.getPlayer().equals(Player.SECOND) && p.getStones().equals(0))
                .collect(Collectors.toList())
                .size();

        return firstPlayerPits.equals(6) || secondPlayerPits.equals(6);
    }

    private Boolean isPitNumberValid(Integer pitNumber) {

        if (pitNumber > 13 || pitNumber < 0) return false;
        if (pitNumber.equals(7) || pitNumber.equals(14)) return false;
        if (turn.equals(Player.FIRST) && pitNumber > 7) return false;
        if (turn.equals(Player.SECOND) && pitNumber < 7) return false;
        if (pits.get(pitNumber - 1).isEmpty()) return false;
        return true;
    }

    private void moveFirstPlayer(Integer pitNumber) {

        Integer pitIndex = pitNumber - 1;
        Pit initialPit = pits.get(pitIndex);
        Integer stones = initialPit.getStones();

        initialPit.setStones(0);
        int currentIndex = pitIndex + 1;


        do {

            if (currentIndex > 12) {
                currentIndex = 0;
            }

            pits.get(currentIndex).increaseStones(1);
            stones = stones - 1;


            currentIndex = currentIndex + 1;


        } while (stones > 0);
    }

    private void moveSecondPlayer(Integer pitNumber) {
        Integer pitIndex = pitNumber - 1;
        Pit initialPit = pits.get(pitIndex);
        Integer stones = initialPit.getStones();

        initialPit.setStones(0);
        int currentIndex = pitIndex + 1;


        do {
            if (currentIndex != 6) {
                if (currentIndex > 13) {
                    currentIndex = 0;
                }

                pits.get(currentIndex).increaseStones(1);
                stones = stones - 1;

            }

            currentIndex = currentIndex + 1;


        } while (stones > 0);

    }

    private Integer findLoopsFirstPlayer(Integer initial, Integer current, Integer stones) {

        return (stones - (current - initial)) / 13;
    }

    private Integer findLoopsFirstPlayerForOverlaps(Integer initial, Integer current, Integer stones) {
        return (stones - 13 + Math.abs(initial - current)) / 13;
    }


}
