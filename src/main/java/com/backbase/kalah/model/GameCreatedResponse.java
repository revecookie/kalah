package com.backbase.kalah.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GameCreatedResponse {

    private Integer id;
    private String uri;

}
