package com.backbase.kalah.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GameOverResponse {

    public static String FIRST_PLAYER_WINS = "Game Over! First player wins with score: ";
    public static String SECOND_PLAYER_WINS = "Game Over! Second player wins with score: ";

    private Integer id;
    private String url;
    private String message;

}
