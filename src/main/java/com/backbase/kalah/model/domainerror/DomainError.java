package com.backbase.kalah.model.domainerror;

public abstract class DomainError extends Exception {

    public abstract String getMessage();
}
