package com.backbase.kalah.model.domainerror;


public class InvalidMoveException extends DomainError {

    @Override
    public String getMessage() {
        return "This move is not valid";
    }
}
