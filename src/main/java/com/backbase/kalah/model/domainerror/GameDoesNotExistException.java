package com.backbase.kalah.model.domainerror;


public class GameDoesNotExistException extends DomainError {

    @Override
    public String getMessage() {
        return "Game does not exist";
    }
}
