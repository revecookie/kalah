package com.backbase.kalah.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Pit {
    private Integer stones = 6;
    private Integer index;
    private Player player;

    public Boolean isEmpty() {
        return stones.equals(0);
    }

    public Boolean isKalah() {
        return index.equals(7) || index.equals(14);
    }

    public void increaseStones(Integer loops) {
        stones = stones + loops;
    }
}
