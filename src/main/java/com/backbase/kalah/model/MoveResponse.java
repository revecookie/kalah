package com.backbase.kalah.model;

import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
public class MoveResponse {
    private Integer id;
    private String url;
    private Map<String, String> status;

    public MoveResponse(Integer id, String url, List<Pit> pits) {
        this.id = id;
        this.url = url;
        this.status = pits.stream().collect(Collectors.toMap(
                p -> String.valueOf(p.getIndex()),
                p -> String.valueOf(p.getStones())));
    }
}
