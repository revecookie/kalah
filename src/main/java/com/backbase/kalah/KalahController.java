package com.backbase.kalah;

import com.backbase.kalah.model.Game;
import com.backbase.kalah.model.GameCreatedResponse;
import com.backbase.kalah.model.GameOverResponse;
import com.backbase.kalah.model.MoveResponse;
import com.backbase.kalah.model.domainerror.DomainError;
import com.backbase.kalah.model.domainerror.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class KalahController {

    @Autowired
    GameStore gameStore;


    @PostMapping("/games")
    public ResponseEntity<GameCreatedResponse> createGame(HttpServletRequest request) {

        Integer gameKey = gameStore.createGame();
        String uri = getHostURL(request) + "/games/" + gameKey;

        return new ResponseEntity<>(new GameCreatedResponse(gameKey, uri), HttpStatus.CREATED);
    }

    @PutMapping("/games/{id}/pits/{pitNumber}")
    public ResponseEntity<?> move(@PathVariable Integer id,
                                  @PathVariable Integer pitNumber,
                                  HttpServletRequest request) {

        String url = getHostURL(request) + "/games/" + id;
        Game game;

        try {
           game = gameStore.move(id, pitNumber);
        } catch (DomainError e) {

            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);

        }

        if(game.isGameOver()) {
            return new ResponseEntity<>(new GameOverResponse(id, url, "message"), HttpStatus.OK);
        }

        return new ResponseEntity<>(new MoveResponse(id, url, game.getPits()), HttpStatus.OK);


    }

    private String getHostURL(HttpServletRequest request) {

        return request.getRequestURL().toString().replace(request.getRequestURI(), "");

    }
}
