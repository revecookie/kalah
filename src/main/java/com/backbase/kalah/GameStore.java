package com.backbase.kalah;

import com.backbase.kalah.model.Game;
import com.backbase.kalah.model.domainerror.GameDoesNotExistException;
import com.backbase.kalah.model.domainerror.InvalidMoveException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class GameStore {

    private Map<Integer, Game> games = new HashMap<>();

    public Integer createGame() {
        Integer gameKey = games.keySet().size() + 1;
        games.put(gameKey, new Game());
        return gameKey;
    }

    public Game move(Integer gameNumber, Integer pitNumber) throws GameDoesNotExistException, InvalidMoveException {

        if (!games.containsKey(gameNumber)) throw new GameDoesNotExistException();

        Game game = games.get(gameNumber);
        game.move(pitNumber);
        return game;

    }
}
