package com.backbase.kalah.model;

import com.backbase.kalah.model.domainerror.DomainError;
import com.backbase.kalah.model.domainerror.InvalidMoveException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GameTest {

    @Test(expected = InvalidMoveException.class)
    public void whenGameIsAlreadyOverMoveShouldBeIllegal() throws DomainError {
        Game game = new Game();
        game.getPits().stream().filter(p -> !p.isKalah()).forEach(p -> p.setStones(0));
        game.getPits().stream().filter(p -> p.isKalah()).forEach(p -> p.setStones(10));
        game.move(3);
    }

    @Test(expected = InvalidMoveException.class)
    public void whenPitNumberIsInvalidMoveShouldBeIllegal() throws DomainError {
        Game game = new Game();
        game.setTurn(Player.SECOND);
        game.move(1);
    }

    @Test(expected = InvalidMoveException.class)
    public void whenPitIsEmptyMoveShouldBeIllegal() throws DomainError {
        Game game = new Game();
        game.getPits().get(0).setStones(0);
        game.move(1);
    }

    @Test(expected = InvalidMoveException.class)
    public void whenIndexDoesNotExistMoveShouldBeIllegal() throws DomainError {
        Game game = new Game();
        game.setTurn(Player.SECOND);
        game.move(16);
    }

    @Test
    public void whenFirstPlayerMovesResultShouldBeCorrect() throws DomainError {
        Game game = new Game();

        game.move(1);

        assertEquals(0, (int) game.getPits().get(0).getStones());
        assertEquals(7, (int) game.getPits().get(1).getStones());
        assertEquals(1, (int) game.getPits().get(6).getStones());
        assertEquals(6, (int) game.getPits().get(7).getStones());
        assertEquals(0, (int) game.getPits().get(13).getStones());
    }

    @Test
    public void whenFirstPlayerMovesStonesThatSurpassTheInitialIndexResultShouldBeCorrect() throws DomainError {
        Game game = new Game();
        game.getPits().get(5).setStones(15);

        game.move(6);

        assertEquals(7, (int) game.getPits().get(0).getStones());
        assertEquals(2, (int) game.getPits().get(6).getStones());
        assertEquals(8, (int) game.getPits().get(7).getStones());
        assertEquals(0, (int) game.getPits().get(13).getStones());
    }

    @Test
    public void whenSecondPlayerMovesResultShouldBeCorrect() throws DomainError {
        Game game = new Game();
        game.setTurn(Player.SECOND);
        game.move(8);

        assertEquals(6, (int) game.getPits().get(0).getStones());
        assertEquals(0, (int) game.getPits().get(7).getStones());
        assertEquals(1, (int) game.getPits().get(13).getStones());
        assertEquals(7, (int) game.getPits().get(12).getStones());
        assertEquals(0, (int) game.getPits().get(6).getStones());
    }

    @Test
    public void whenSecondPlayerMovesStonesThatSurpassTheInitialIndexResultShouldBeCorrect() throws DomainError {
        Game game = new Game();
        game.getPits().get(8).setStones(27);
        game.setTurn(Player.SECOND);

        game.move(9);

        assertEquals(8, (int) game.getPits().get(0).getStones());
        assertEquals(8, (int) game.getPits().get(1).getStones());
        assertEquals(0, (int) game.getPits().get(6).getStones());
        assertEquals(9, (int) game.getPits().get(9).getStones());
        assertEquals(8, (int) game.getPits().get(10).getStones());
        assertEquals(2, (int) game.getPits().get(8).getStones());
        assertEquals(2, (int) game.getPits().get(13).getStones());
    }

}