package com.backbase.kalah;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class KalahControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createsNewGame() throws Exception {
        initializeGame();
    }

    @Test
    public void whenTryingToStartPlayingFromAKalahYouShouldGetAnError() throws Exception {

        initializeGame();

        mockMvc.perform(put("/games/1/pits/7"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error").value("This move is not valid"));

        mockMvc.perform(put("/games/1/pits/14"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error").value("This move is not valid"));


    }

    @Test
    public void whenTryingToPickStonesFromAnOpponentsPitYouShouldGetAnError() throws Exception {
        initializeGame();

        mockMvc.perform(put("/games/1/pits/9"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error").value("This move is not valid"));

    }

    @Test
    public void whenTryingAValidMoveYouShouldGetBackTheStatus() throws Exception {
        initializeGame();

        mockMvc.perform(put("/games/1/pits/3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.3").value(0))
                .andExpect(jsonPath("$.status.1").value(6))
                .andExpect(jsonPath("$.status.4").value(7))
                .andExpect(jsonPath("$.status.7").value(1))
                .andExpect(jsonPath("$.url").value("http://localhost/games/1"));
    }

    public void initializeGame() throws Exception {
        mockMvc.perform(post("/games")).andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.uri").value("http://localhost/games/1"));
    }

}