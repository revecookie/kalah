# Kalah game

## Prerequisites
In order to run the application you will need
1. Java 8 [How to install java 8](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html)
2. Gradle (If not using intellij) [How to install gradle](https://gradle.org/install/)

## How to run

To start the server, while you are in the root folder of the project, type:

`./gradlew bootrun`

To run the tests, again while in the root folder of the project, type:

`./gradlew test`

# How to play
You can use the following example curl commands to play:

``` bash
curl --header "Content-Type: application/json" \
--request POST \
http://localhost:8085/games
```

``` bash
curl --header "Content-Type: application/json" \
--request PUT \ 
http://localhost:8085/games/1/pits/3
```


## Decisions, Assumptions and Notes
1. All the games are stored in memory
2. There is no time limit
4. We return 400 at any invalid move
5. There is no player authentication. The request order defines who is the one playing


## Future improvements

- [ ] Introduce in memory store to handle the games
- [ ] Make error handling more specific and remove specific classes (Use only domain error with specific string)
- [ ] Cleanup logic a bit
- [ ] Make it generic (for x amount of stones)
- [ ] Create player authentication, so that each player can only play in their own turn


